#!/usr/bin/env sh
cd $(git rev-parse --show-toplevel)
poetry run flake8 --config=utils/flake8 .
poetry run pylint --rcfile=utils/pylintrc rapvs
poetry run python -m markdown2 README.md > README.html
