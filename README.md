# rapvs - Remote Access Point Verwaltungssystem

RAPVS dient zur Verwaltung von Remote Accesspoints am KIT.

## Entwicklung

Zur venv-Verwaltung wird [poetry](https://python-poetry.org) genutzt.
Mit `poetry install` kann die Umgebung vorbereitet werden.
Mit `poetry shell` kann man auf der Shell in die Umgebung wechseln.
Mit `poetry run <cmd>` kann ein Kommando in der Umgebung ausgeführt werden.

## DB-Migrationen

Wir nutzen alembic. Die config liegt unter `rapvs/db/alembic.ini`.
Commands müssen also mit `alembic -c rapvs/db/alembic.ini ...` im hauptverzeichnis ausgeführt werden.

## Dokumente
Weitere zum RAP-Prozess zugehörige Dokumente sind im Repository [rapvs-documents](https://gitlab.net.scc.kit.edu/scc-net/rapvs-documents) zu finden.
Dort befindet sich auch eine Kopie der TeX-Dateien, zum schnellen Testen und Kompilieren.
