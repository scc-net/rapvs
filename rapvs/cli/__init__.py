"""Specifies Command Line Interface"""

import os
import sys
from pathlib import Path

import alembic.command
import click
import yaml
from alembic import script
from alembic.config import Config
from alembic.migration import MigrationContext
from sqlalchemy.exc import OperationalError

from ._utils.core import DynamicMultiCommandFactory
from ._utils.helpers import convert_time_expression_to_seconds
from ..db.db import DB

CONTEXT_SETTINGS = {'help_option_names': ['-h', '--help']}

DynamicMultiCommand = DynamicMultiCommandFactory().create(__file__, __package__)
user_config_dir = Path(click.get_app_dir('rapvs')) / 'config.yml'


@click.command(cls=DynamicMultiCommand, context_settings=CONTEXT_SETTINGS)
@click.option('--config', '-c', type=click.Path(exists=True, dir_okay=False),
              help=f'Config file path. '
                   f'RAPVS checks automatically in  [/etc/rapvs.yml, {user_config_dir}]. '
                   f'The first found is used.')
@click.option('--debug', '-d', is_flag=True, help='Enable python tracebacks.')
@click.pass_context
def cli(ctx, config, debug):
    """rapvs

    A cli tool to manage RAPs at KIT.
    See subcommands for further informations.
    """
    if not debug:
        sys.tracebacklimit = 0

    if config is None:
        for configpath in ['/etc/rapvs.yml', user_config_dir]:
            if Path(configpath).expanduser().is_file():
                config = configpath

    if config is None:
        raise click.ClickException(f'No config file found. Pass it with "-c / --config" '
                                   f'or place it under "/etc/rapvs.yml" or "{user_config_dir}".')

    with open(Path(config).expanduser(), 'r', encoding='utf-8') as configfile:
        config_dict = yaml.safe_load(configfile)

    try:
        db_host = config_dict['postgresql'].get('host')
        db_port = config_dict['postgresql'].get('port')
        db_database = config_dict['postgresql']['database']
        db_user = config_dict['postgresql']['username']
        db_password = config_dict['postgresql']['password']
    except KeyError as error:
        raise click.ClickException(f"Missing Key in Config Section 'postgresql': {error}.") from error

    try:
        db_connection = DB(db_user, db_password, db_database, db_host, db_port)
    except OperationalError as error:
        raise click.ClickException(f"Can not connect to Postgresql DB:{os.linesep}{error}") from error

    module_path = Path(__file__).parents[1]
    alembic_cfg = Config(module_path / "db" / "alembic.ini")
    alembic_cfg.set_section_option("alembic", "script_location", str(module_path / "db" / "migrations"))
    directory = script.ScriptDirectory.from_config(alembic_cfg)

    engine = db_connection.engine
    conn = engine.connect()

    context = MigrationContext.configure(conn)
    if not set(context.get_current_heads()) == set(directory.get_heads()):
        alembic.command.upgrade(alembic_cfg, "head")

    for key in config_dict['expiry_periods']:
        config_dict['expiry_periods'][key] = convert_time_expression_to_seconds(config_dict['expiry_periods'][key])

    ctx.obj = {
        'CONFIG': config_dict,
        'DB': db_connection,
    }
