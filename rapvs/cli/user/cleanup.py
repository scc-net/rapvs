"""Implement a cli module to delete users not needed any more"""
import click

from ...system import user


@click.command(name='cleanup')
@click.pass_context
def cli(ctx):
    """Delete users not needed any more

    \b
    """

    db_connection = ctx.obj['DB']

    user_db = user.cleanup(db_connection)

    click.echo(f"Deleted {len(user_db)} user")
