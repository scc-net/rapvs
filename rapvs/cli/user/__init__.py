"""cli module for operations on users"""

import click

from .. import DynamicMultiCommandFactory

DynamicMultiCommand = DynamicMultiCommandFactory().create(__file__, __package__)


@click.command(cls=DynamicMultiCommand, name='user', short_help='manage users')
def cli():
    """Manage users

    This sub module allows to delete users whose rejection timestamp of the application is long time ago.
    """
