"""Implement a cli module to list profiles"""

import click

from .._utils.helpers import cond_echo_via_pager
from ...system import profile


@click.command(name='list')
@click.pass_context
def cli(ctx):
    """List profiles"""

    db_connection = ctx.obj['DB']  # pylint: disable=C0103
    profile_list = profile.list_(db_connection)
    headers = ['Profile Name', 'VLAN ID']

    cond_echo_via_pager(profile_list, headers)
