"""Implement a cli module to view profiles"""

import click

from ...system import profile as profile_

MESSAGE = """name: {}
VLAN ID: {}"""


@click.command(name='view')
@click.argument('profile')
@click.pass_context
def cli(ctx, profile):
    """Create an application

    \b
    Parameters:
    profile    profile name
    """

    db_connection = ctx.obj['DB']
    profile_db = profile_.view(db_connection, profile)
    click.echo(MESSAGE.format(*profile_db))
