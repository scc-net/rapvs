"""cli module to manage RAP profiles"""

import click

from .. import DynamicMultiCommandFactory

DynamicMultiCommand = DynamicMultiCommandFactory().create(__file__, __package__)


@click.command(cls=DynamicMultiCommand, name='profile', short_help='manage RAP profiles')
def cli():
    """Manage RAP profiles

    This sub module allows to create, list or view profiles.
    """
