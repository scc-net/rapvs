"""Helper to format output"""

from tabulate import tabulate


def pretty_table(data: list, headers: list = None, tablefmt: str = 'github') -> str:
    """Tabulate wrapper function with predefined format"""
    if headers is None:
        table = tabulate(data, tablefmt=tablefmt)
    else:
        table = tabulate(data, headers=headers, tablefmt=tablefmt)
    return table
