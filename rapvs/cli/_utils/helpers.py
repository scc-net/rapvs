"""click helper functions"""
import sys
import typing as t

import cups
from click import echo, secho, echo_via_pager, Parameter
from click.exceptions import ClickException, BadParameter
from netdb_client.api32 import APISession, APIEndpoint

from .format import pretty_table
from .interactive import printer_prompt
from ...system import ldap


def init_netdb(netdb_config: dict) -> APISession:
    """Initialise NetDB Session"""
    try:
        netdb_host = netdb_config['host']
        netdb_token = netdb_config['token']
    except KeyError as error:
        raise ClickException(f"Missing Key in Config Section 'netdb': {error}.") from error

    netdb_endpoint = APIEndpoint(base_url=netdb_host, version="3.0", token=netdb_token)
    netdb_session = APISession(netdb_endpoint)
    return netdb_session


def init_ldap(ldap_config: dict) -> ldap.LDAPConnection:
    """Initialise LDAP Connection"""
    try:
        ldap_uri = ldap_config['uri']
        ldap_username = ldap_config['username']
        ldap_password = ldap_config['password']
    except KeyError as error:
        raise ClickException(f"Missing Key in Config Section 'ldap': {error}.") from error

    ldap_connection = ldap.LDAPConnection(ldap_uri, ldap_username, ldap_password)
    return ldap_connection


def init_cups(print_config: dict) -> (dict, cups.Connection):
    """Initialise CUPS Connection"""
    try:
        cups_server = print_config['server']
    except KeyError as error:
        raise ClickException(f"Missing Key in Config Section 'print': {error}.") from error

    cups.setServer(cups_server)
    cups_connection = cups.Connection()
    printers = {key: value['printer-info'] for key, value in cups_connection.getPrinters().items()}

    return printers, cups_connection


def cond_echo_via_pager(list_: list, headers: list):
    """Echos in pager if list is to long (15 items)"""
    table = pretty_table(list_, headers)
    if len(list_) > 15:
        echo_via_pager(table, )
    else:
        echo(table)


def select_printer(printers: dict, printer: str, parameter: Parameter) -> str:
    """Prompt for interactive printer selection if no printer was provided"""
    if not printers:
        raise ClickException("No printers available, please check your cups config.")

    if printer is None:
        printer = printer_prompt(printers)
        if printer is None:
            secho('Abort printing.', fg='red', bold=True)
            sys.exit(0)

    # C0209: consider-using-f-string
    # pylint: disable=C0209
    if printer not in printers:
        raise BadParameter(
            "'{printer}' is not one of '{printers}'".format(
                printer=printer, printers="', '".join(printers.keys())
            ), param=parameter
        )

    return printer


def convert_time_expression_to_seconds(value: t.Union[int, str]) -> int:
    """Convert a duration as a string or integer to a number of seconds.

        If an integer is provided it is treated as seconds and is unchanged.

        String durations can have a suffix of 's', 'm', 'h', 'd', 'w', or 'y'.
        No suffix is treated as seconds.
        """
    if isinstance(value, int):
        return value

    if isinstance(value, str) and value.isdigit():
        return int(value)

    second = 1
    minute = 60 * second
    hour = 60 * minute
    day = 24 * hour
    week = 7 * day
    year = 365 * day
    sizes = {'s': second, 'm': minute, 'h': hour, 'd': day, 'w': week, 'y': year}
    suffix = value[-1]

    if suffix in sizes:
        value = value[:-1]
        size = sizes[suffix]
    else:
        raise ClickException(f'{value!r} is not a valid time duration expression')
    return int(value) * size


def unpacking_get_data_application(application_, user, ou):
    if user is None:
        raise ClickException("User is None, application outdated")
    application_final = {"id": application_.id, "user_name": user.name, "user_account": user.account,
                         "user_email": user.email, "ou_name": ou.name, "ou_abbreviation": ou.abbreviation,
                         "creation_timestamp": application_.creation_timestamp}
    return application_final


def unpacking_get_data_assignment(assignment_, user, ou, _):
    if user is None:
        raise ClickException("User is None, assignment revoked")
    assignment_final = {"id": assignment_.id, "creation_timestamp": assignment_.creation_timestamp,
                        "user_name": user.name, "user_account": user.account, "ou_name": ou.name,
                        "ou_abbreviation": ou.abbreviation,
                        "mac": assignment_.rap_mac, "serial_number": assignment_.rap_serial_number}
    return assignment_final
