"""Implements interactive dialogues for cli interface"""

import inquirer


def printer_prompt(printers: dict) -> str:
    """Create printer dialog
    """
    printer_choices = {f'{key} - {value}': key for key, value in printers.items()}
    printer_choices['Abort'] = None
    prompt = inquirer.prompt([inquirer.List(
        name='printer', message='Select printer', choices=printer_choices.keys()
    )])
    if prompt is not None:
        printer = printer_choices[prompt['printer']]
    else:
        printer = None

    return printer
