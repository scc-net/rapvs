"""Implements custom click types."""

import re

from click import ParamType


class MAC(ParamType):
    """The MAC type converts input to a normalized mac address"""

    name = 'MAC'

    def convert(self, value, param, ctx):
        mac_raw = value.lower().strip()
        mac_filtered = re.sub(r'(.*?)([0-9a-f]+|$|^)', lambda m: ('' * len(m.groups()[0]) + m.groups()[1]), mac_raw)
        if not len(mac_filtered) == 12:
            self.fail(f'{value} is not a valid MAC address')
        mac_formatted = ''.join(':' + c if i % 2 == 0 and not i == 0 else c for i, c in enumerate(mac_filtered, 0))
        return mac_formatted
