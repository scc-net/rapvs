"""Implement a cli module to replace rap"""

import click

from .printout import cli as printout
from .._utils.types import MAC
from ...system import assignment


@click.command(name='withdraw')
@click.option('--old-mac', '-om', metavar='MAC', type=MAC(), help='MAC of the old RAP',
              prompt='Please enter the old RAPs MAC address')
@click.option('--new-mac', '-nm', metavar='MAC', type=MAC(), help='MAC of the new RAP',
              prompt='Please enter the new RAPs MAC address')
@click.option('--new-serial-number', '-ns', metavar='S/N', help='S/N of the new RAP',
              prompt='Please enter the new RAPs S/N')
@click.option('--printer', metavar='PRINTER', type=click.STRING, help='Printer to use')
@click.pass_context
def cli(ctx, old_mac, new_mac, new_serial_number, printer):
    """Replace RAP from assignment"""

    db_connection = ctx.obj['DB']

    assignment_id = assignment.replace_rap(db_connection, old_mac, new_mac, new_serial_number)
    click.secho(f'Replaced RAP for Assignment {assignment_id}\n', fg='green')
    click.echo(f'Old MAC: {old_mac}\n'
               f'New MAC: {new_mac}\n'
               f'New S/N: {new_serial_number}\n')
    click.secho('Please return the old RAP-receipt signed (by you) to the user\n'
                'and get the new receipt signed by the user.\n',
                fg='red', bold=True)

    ctx.invoke(printout, id_=assignment_id, printer=printer)
