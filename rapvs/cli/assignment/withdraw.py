"""Implement a cli module to withdraw assignments"""

import click

from ...system import assignment


@click.command(name='withdraw')
@click.argument('rap_mac', metavar='MAC')
@click.pass_context
def cli(ctx, rap_mac):
    """Withdraw an assignment

    \b
    Parameters:
    MAC  assigned RAP MAC
    """

    db_connection = ctx.obj['DB']

    withdrawn_assignment = assignment.withdraw(db_connection, rap_mac)
    click.echo(f'Assignment {withdrawn_assignment.id} withdrawn.')
