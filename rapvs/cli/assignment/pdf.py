"""Implement a cli module to save application pdfs"""
import os.path

import click
from sqlalchemy.exc import NoResultFound

from ...system import assignment, pdf
from .._utils.helpers import unpacking_get_data_assignment


@click.command(name='pdf')
@click.argument('id_', metavar='ID', type=click.INT)
@click.option('--output', type=click.Path(exists=True, file_okay=False),
              default=os.getcwd(), show_default='current working dir')
@click.pass_context
def cli(ctx, id_, output):
    """Get assignment pdf

    \b
    Parameters:
    ID  application ID
    """
    db_connection = ctx.obj['DB']
    try:
        (assignment_, user, ou, profile) = assignment.get_data(db_connection, id_)
    except NoResultFound as error:
        raise click.ClickException(f"Assignment {id_} not found") from error
    assignment_final = unpacking_get_data_assignment(assignment_, user, ou, profile)
    document = pdf.compile_assignment(assignment_final)
    filename = f'{assignment_final["id"]:0>5d}_assignment_{assignment_final["user_account"]}.pdf'
    document.save_to(os.path.join(output, filename))
