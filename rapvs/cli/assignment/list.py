"""Implement a cli module to list assignments"""

import click

from .._utils.helpers import cond_echo_via_pager
from ...system import assignment


@click.command(name='list')
@click.option('--all', 'list_all', is_flag=True)
@click.pass_context
def cli(ctx, list_all):
    """List assignments"""

    db_connection = ctx.obj['DB']
    assignment_list = assignment.list_(db_connection, list_all)
    headers = ['ID', 'Creation Timestamp', 'Revoke Timestamp', 'User Account', 'MAC', 'Profile', 'Application ID']

    cond_echo_via_pager(assignment_list, headers)
