"""Implement a cli module to set rap profile for assignment"""

import click

from ...system import assignment


@click.command(name='profile')
@click.option('--profile', '-p', metavar='PROFILE', help='RAP profile')
@click.argument('id_', metavar='ID')
@click.pass_context
def cli(ctx, profile, id_):
    """Set rap profile for assignment

    \b
    Parameters:
    ID  assignment ID
    """

    db_connection = ctx.obj['DB']

    if profile == '':
        profile = None

    assignment.profile(db_connection, id_, profile)
