"""Implement a cli module to print assignment pdfs"""

import click
from sqlalchemy.exc import NoResultFound

from rapvs.system import assignment, pdf, printout
from .._utils.helpers import init_cups, select_printer, unpacking_get_data_assignment


@click.command(name='printout')
@click.argument('id_', metavar='ID')
@click.option('--printer', '-p', metavar='PRINTER', help='Printer to use', type=click.STRING)
@click.pass_context
def cli(ctx, id_, printer):
    """Print an assignment

    \b
    Parameters:
    ID  assignment ID
    """

    db_connection = ctx.obj['DB']
    printers, cups_connection = init_cups(click.get_current_context().obj['CONFIG']['print'])

    if not printers:
        raise click.ClickException("No printers available, please check your cups config.")

    printer = select_printer(printers, printer, cli.params[1])

    try:
        (assignment_, user, ou, profile) = assignment.get_data(db_connection, id_)
    except NoResultFound as error:
        raise click.ClickException(f"Assignment {id_} not found") from error
    assignment_final = unpacking_get_data_assignment(assignment_, user, ou, profile)
    document = pdf.compile_assignment(assignment_final)

    click.echo(f'Printing Assignment {id_} printout on printer "{printer}" ({printers[printer]}).')
    printout.print_data(cups_connection, printer, document)
