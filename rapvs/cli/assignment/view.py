"""Implement a cli module to view assignments"""

import click
from sqlalchemy.exc import NoResultFound

from ...system import assignment

MESSAGE = """ID: {assignment_id}

Assignment data
Create date: {creation_timestamp}
Reject date: {revoke_timestamp}
RAP mac: {rap_mac}
RAP S/N: {rap_serial_number}
RAP profile: {profile}
"""


@click.command(name='view')
@click.argument('id_', metavar='ID')
@click.pass_context
def cli(ctx, id_):
    """View an assignment

    \b
    Parameters:
    ID  assignment ID
    """

    db_connection = ctx.obj['DB']

    try:
        (assignment_, user, ou, profile_name) = assignment.get_data(db_connection, id_)
    except NoResultFound as error:
        raise click.ClickException(f"Assignment {id_} not found") from error

    click.echo(MESSAGE.format(assignment_id=assignment_.id, creation_timestamp=assignment_.creation_timestamp,
                              revoke_timestamp=assignment_.revoke_timestamp, rap_mac=assignment_.rap_mac,
                              rap_serial_number=assignment_.rap_serial_number, profile=profile_name))
