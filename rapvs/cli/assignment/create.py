"""Implement a cli module to create assignments"""

import click

from .printout import cli as printout
from .._utils.types import MAC
from ...system import assignment


# R0913: ignore too many arguments
# pylint: disable=R0913
@click.command(name='create')
@click.argument('id_', metavar='ID of application', type=click.INT)
@click.option('--mac', '-m', metavar='MAC', type=MAC(), help='MAC of RAP', prompt='Please enter RAP MAC address')
@click.option('--serial-number', '-s', metavar='S/N', help='S/N of RAP', prompt='Please enter RAP S/N')
@click.option('--profile', '-p', metavar='PROFILE', help='RAP profile')
@click.option('--printer', metavar='PRINTER', type=click.STRING, help='Printer to use')
@click.pass_context
def cli(ctx, id_, mac, serial_number, profile, printer):
    """Create an assignment from application

    \b
    Parameters:
    ID  application ID
    """

    db_connection = ctx.obj['DB']
    assignment_id = assignment.create(db_connection, id_, mac, serial_number, profile)

    ctx.invoke(printout, id_=assignment_id, printer=printer)
