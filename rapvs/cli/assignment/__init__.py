"""cli module for operations on assignments"""

import click

from .. import DynamicMultiCommandFactory

DynamicMultiCommand = DynamicMultiCommandFactory().create(__file__, __package__)


@click.command(cls=DynamicMultiCommand, name='assignment', short_help='manage assignments')
def cli():
    """Manage assignments

    This sub module allows to list, view or withdraw assignments and change RAP profiles for an assignment.
    """
