"""cli module to manage OUs"""

import click

from .. import DynamicMultiCommandFactory

DynamicMultiCommand = DynamicMultiCommandFactory().create(__file__, __package__)


@click.command(cls=DynamicMultiCommand, name='ou', short_help='manage OUs')
def cli():
    """Manage Ous

    This sub module allows to add or list OUs.
    """
