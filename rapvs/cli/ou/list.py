"""Implement a cli module to list OUs"""

import click

from .._utils.helpers import cond_echo_via_pager
from ...system import organizational_unit as ou


@click.command(name='list')
@click.pass_context
def cli(ctx):
    """List organizational units"""

    db_connection = ctx.obj['DB']
    ou_list = ou.list_(db_connection)
    headers = ['Abbreviation', 'Name']

    cond_echo_via_pager(ou_list, headers)
