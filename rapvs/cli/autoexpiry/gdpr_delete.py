"""Implement a cli module to delete user data when RAP return or application rejection was longer than 6 months"""
import datetime

import click

from ...system import application, assignment


@click.command(name='gdpr_delete')
@click.pass_context
def cli(ctx):
    """Delete user data from RAPs that are long returned or applications rejected long time ago

    \b
    """

    db_connection = ctx.obj['DB']

    # Check for long rejected applications
    application_db = application.list_(db_connection)
    application_list = [
        (application_intern.id, application_intern.rejected_timestamp, application_intern.user_id,
         application_intern.is_assigned, application_intern.is_rejected)
        for application_intern, ou, user_app, assignment_app in application_db]

    actual_time = datetime.datetime.now()
    try:
        time_over = ctx.obj['CONFIG']['expiry_periods']['gdpr_deletion']
    except KeyError as error:
        raise click.ClickException(f"Missing key {error} in config file")

    for application_id, rejected_timestamp, _, _, is_rejected in application_list:
        if is_rejected and rejected_timestamp + datetime.timedelta(seconds=time_over) < actual_time:
            application.remove_user(db_connection, application_id)

    # Check for long returned RAPs
    assignment_db = assignment.list_(db_connection, list_all=True)

    for _, _, assignment_revoke_timestamp, _, _, _, assignment_application_id in assignment_db:
        if assignment_revoke_timestamp is not None and assignment_revoke_timestamp + datetime.timedelta(
                seconds=time_over) < actual_time:
            application.remove_user(db_connection, assignment_application_id)
