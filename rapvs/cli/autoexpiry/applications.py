"""Implement a cli module to reject applications"""
import datetime

import click

from .._utils.helpers import unpacking_get_data_application
from ...system import application, email


@click.command(name='applications')
@click.option('--send-mail/--send-no-mail', 'send_mail', is_flag=True, prompt='Send rejection mail to user?')
@click.option('--yes', '-y', 'confirm_all', is_flag=True,
              help='Reject all applications without asking for confirmation.')
@click.pass_context
def cli(ctx, send_mail, confirm_all):
    """Reject old applications that are not yet assigned

    \b
    """

    db_connection = ctx.obj['DB']

    application_db = application.list_(db_connection)
    application_list = [
        (application_intern.id, application_intern.creation_timestamp, application_intern.is_assigned,
         application_intern.is_rejected, ou.abbreviation, user.name)
        for application_intern, ou, user, assignment in application_db]

    actual_time = datetime.datetime.now()
    try:
        time_over = ctx.obj['CONFIG']['expiry_periods']['application']
    except KeyError as error:
        raise click.ClickException(f"Missing key {error} in config file")

    for application_id, creation_timestamp, is_assigned, is_rejected, ou_short, user_name in application_list:
        if not is_assigned and not is_rejected and (
                (creation_timestamp + datetime.timedelta(seconds=time_over)) < actual_time):
            if not confirm_all:
                confirm = click.confirm(f"Reject Application {application_id}/{user_name} ({ou_short}) "
                                        f"created at {creation_timestamp}?")
                if not confirm:
                    continue
            try:
                application.reject(db_connection, application_id)
                click.echo(f"Rejected Application: {application_id}/{user_name} ({ou_short})")
                if send_mail:
                    (application_, user, ou) = application.get_data(db_connection, application_id)
                    application_mail = unpacking_get_data_application(application_, user, ou)
                    mail = email.application_rejected_mail(time_over)
                    email.send_mail(mail, ctx.obj['CONFIG']['mail'], application_mail)
            except application.RejectError as error:
                raise click.ClickException(error) from error
