"""cli module for auto-expiry of applications and assignments"""

import click

from .. import DynamicMultiCommandFactory

DynamicMultiCommand = DynamicMultiCommandFactory().create(__file__, __package__)


@click.command(cls=DynamicMultiCommand, name='autoexpiry', short_help='delete expired applications and assignments')
def cli():
    """Delete expired applications and assignments

    This sub module allows to automatically revoke expired applications and remove and or delete users of old
    revoked applications or returned assignments.
    """
