"""Synchronize RAPVS with Aruba Mobility Conductor"""

import click

from ...system.mobility_conductor import ArubaAPI
from ...system.sync import mobility_conductor


@click.command(name='mc', short_help='sync RAPVS with Aruba MC')
@click.option('-t', '--test', default=False, is_flag=True)
@click.pass_context
def cli(ctx, test):
    """Synchronize RAPVS with Aruba Mobility Conductor"""
    if test:
        click.secho('RUNNING IN TEST MODE. NO ACTIONS WILL BE PERFORMED', fg='red')

    db_connection = ctx.obj['DB']
    try:
        default_group = ctx.obj['CONFIG']['mobilityconductor']['default_group']
        api = ArubaAPI(ctx.obj['CONFIG']['mobilityconductor'])
    except KeyError as err:
        raise click.ClickException(f'Missing Key in Config Section "mobilityconductor": {err}') from err

    mobility_conductor(db_connection, api, default_group, test, click.echo)
