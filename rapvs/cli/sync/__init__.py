"""cli module for syncing RAPVS with external services"""

import click

from .. import DynamicMultiCommandFactory

DynamicMultiCommand = DynamicMultiCommandFactory().create(__file__, __package__)


@click.command(cls=DynamicMultiCommand, name='sync', short_help='sync rapvs with ext. services')
def cli():
    """Synchronize RAPVS with external services.
    """
