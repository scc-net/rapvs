"""Implement a cli module to reject applications"""

import click

from ...system import application, email
from .._utils.helpers import unpacking_get_data_application


@click.command(name='reject')
@click.argument('id_', metavar='ID')
@click.option('--send-mail/--send-no-mail', 'send_mail', is_flag=True, prompt='Send rejection mail to user?',
              help='Send rejection mail to User.')
@click.option('--yes', '-y', 'confirm', is_flag=True,
              help='Reject application without asking for confirmation.')
@click.pass_context
def cli(ctx, id_, send_mail, confirm):
    """Reject an application

    \b
    Parameters:
    ID  application ID
    """

    db_connection = ctx.obj['DB']

    (application_, user, ou) = application.get_data(db_connection, id_)

    if not confirm:
        confirm = click.confirm(f'Reject application {id_} / {user.name} ({ou.abbreviation})?')

    if confirm:
        try:
            application.reject(db_connection, id_)
            click.echo(f"Rejected Application: {id_}/{user.name} ({ou.abbreviation})")
            if send_mail:
                application_mail = unpacking_get_data_application(application_, user, ou)
                mail = email.application_rejected_mail(None)
                email.send_mail(mail, ctx.obj['CONFIG']['mail'], application_mail)
        except application.RejectError as error:
            raise click.ClickException(str(error)) from error
