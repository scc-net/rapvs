"""Implement a cli module to list applications"""

import click

from .._utils.helpers import cond_echo_via_pager
from ...system import application


@click.command(name='list')
@click.pass_context
def cli(ctx):
    """List applications"""

    db_connection = ctx.obj['DB']
    application_db = application.list_(db_connection)
    application_list = [
        (application_intern.id, ou_name.name, user.account, user.name,
         'application is rejected' if application_intern.rejected_timestamp else (
             ('assigned' if assignment.active else 'assignment withdrawn') if application_intern.is_assigned else ''))
        for application_intern, ou_name, user, assignment in
        application_db]
    headers = ['ID', 'Organization Unit Name', 'User Account', 'User Name', 'Status']

    application_list_sorted = sorted(application_list)

    cond_echo_via_pager(application_list_sorted, headers)
