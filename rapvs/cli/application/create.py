"""Implement a cli module to create applications"""

import click

from .send import cli as send
from .._utils.format import pretty_table
from .._utils.helpers import init_netdb, init_ldap
from ...system import application
from ...system import organizational_unit as ou_

ASSIGNMENT = """organizational_unit: {}
user_account: {}"""

VALUES = {'organizational_unit', 'user_account'}


# pylint: disable=R0912,R0914,R0915
@click.command(name='create')
@click.option('--user-account', '-u', metavar='ACCOUNT', default='',
              help='KIT Account for which the applications is created', required=True, prompt=True)
@click.option('--organizational-unit', '-ou', metavar='OU', default='',
              help='Organizational Unit from which the application comes', required=True, prompt=True)
@click.pass_context
def cli(ctx, user_account, organizational_unit):
    """Create an application"""
    try:
        ldap_basedn = ctx.obj['CONFIG']['ldap']['basedn']
    except KeyError as error:
        raise click.ClickException(f"Missing Key in Config Section 'ldap': {error}.") from error

    db_connection = ctx.obj['DB']
    netdb_session = init_netdb(ctx.obj['CONFIG']['netdb'])
    ldap_connection = init_ldap(ctx.obj['CONFIG']['ldap'])

    ldap_user_data = ldap_connection.account2valid_user(ldap_basedn, user_account)
    user_name = ldap_user_data['name']
    user_email = ldap_user_data['email']

    org = ou_.add(db_connection, netdb_session, organizational_unit)

    click.echo('Application Data:')
    click.echo(pretty_table([('User account:', user_account),
                             ('User name:', user_name),
                             ('OU abbreviation:', org.abbreviation),
                             ('OU name:', org.name)], tablefmt='plain'))
    click.confirm('Create application?', abort=True)

    application_ = application.create(db_connection, user_name, user_account, user_email, org.abbreviation)
    click.echo(f"Application ID: {application_.id}")

    if click.confirm('Send application mail to user?', default=True):
        ctx.invoke(send, id_=application_.id)
