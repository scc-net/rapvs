"""cli module for operations on applications"""

import click

from .. import DynamicMultiCommandFactory

DynamicMultiCommand = DynamicMultiCommandFactory().create(__file__, __package__)


@click.command(cls=DynamicMultiCommand, name='application', short_help='manage applications')
def cli():
    """Manage applications

    This sub module allows to create, list, view, print, accept, comment or reject applications.
    """
