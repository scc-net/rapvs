"""Implement a cli module to send application pdfs to user"""
import click

from ...system import application, pdf, email
from .._utils.helpers import unpacking_get_data_application


@click.command(name='send')
@click.argument('id_', metavar='ID', type=click.INT)
@click.pass_context
def cli(ctx, id_):
    """Send Application PDF via mail to user

    \b
    Parameters:
    ID  application ID
    """
    db_connection = ctx.obj['DB']

    (application_, user, ou) = application.get_data(db_connection, id_)
    application_final = unpacking_get_data_application(application_, user, ou)
    document = pdf.compile_application(application_final)
    mail = email.application_mail(application_final, document)
    email.send_mail(mail, ctx.obj['CONFIG']['mail'], application_final)
    click.echo(f'Sent mail for application {application_final["id"]}')
