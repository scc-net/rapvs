"""Implement a cli module to save application pdfs"""
import os.path

import click

from ...system import application, pdf
from .._utils.helpers import unpacking_get_data_application


@click.command(name='pdf')
@click.argument('id_', metavar='ID', type=click.INT)
@click.option('--output', type=click.Path(exists=True, file_okay=False),
              default=os.getcwd(), show_default='current working dir')
@click.pass_context
def cli(ctx, id_, output):
    """Get Application pdf

    \b
    Parameters:
    ID  application ID
    """
    db_connection = ctx.obj['DB']

    (application_, user, ou) = application.get_data(db_connection, id_)
    application_final = unpacking_get_data_application(application_, user, ou)
    document = pdf.compile_application(application_final)
    filename = f'{application_final["id"]:0>5d}_application_{application_final["user_account"]}.pdf'
    document.save_to(os.path.join(output, filename))
