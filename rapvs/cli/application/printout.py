"""Implement a cli module to print application pdfs"""

import click

from rapvs.system import application, pdf, printout
from .._utils.helpers import init_cups, select_printer, unpacking_get_data_application


@click.command(name='printout')
@click.argument('id_', metavar='ID')
@click.option('--printer', '-p', metavar='PRINTER', help='Printer to use', type=click.STRING)
@click.pass_context
def cli(ctx, id_, printer):
    """Print an application

    \b
    Parameters:
    ID  application ID
    """

    db_connection = ctx.obj['DB']
    printers, cups_connection = init_cups(click.get_current_context().obj['CONFIG']['print'])

    if not printers:
        raise click.ClickException("No printers available, please check your cups config.")

    printer = select_printer(printers, printer, cli.params[1])

    (application_, user, ou) = application.get_data(db_connection, id_)
    application_final = unpacking_get_data_application(application_, user, ou)
    document = pdf.compile_application(application_final)

    click.echo(f'Printing Application {id_} printout on printer "{printer}" ({printers[printer]}).')
    printout.print_data(cups_connection, printer, document)
