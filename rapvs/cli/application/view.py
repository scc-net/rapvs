"""Implement a cli module to view applications"""

import click

from ...system import application

MESSAGE = """ID: {application_id}

Application data
User name: {user_name}
User account: {user_account}
OU: {ou_name}
OU abbreviation: {ou_abbreviation}
Create date: {creation_timestamp}
Reject date: {rejected_timestamp}
"""


@click.command(name='view')
@click.argument('id_', metavar='ID', type=click.INT)
@click.pass_context
def cli(ctx, id_):
    """View an application

    \b
    Parameters:
    ID  application ID
    """
    db_connection = ctx.obj['DB']
    (application_, user, ou) = application.get_data(db_connection, id_)
    click.echo(MESSAGE.format(application_id=application_.id, user_account=user.account if user is not None else "None",
                              user_name=user.name if user is not None else "None", ou_abbreviation=ou.abbreviation,
                              ou_name=ou.name, creation_timestamp=application_.creation_timestamp,
                              rejected_timestamp=application_.rejected_timestamp))
