"""cli module for operations on remote access points"""

import click

from .. import DynamicMultiCommandFactory

DynamicMultiCommand = DynamicMultiCommandFactory().create(__file__, __package__)


@click.command(cls=DynamicMultiCommand, name='rap', short_help='manage raps')
def cli():
    """Manage remote access points

    This sub module allows to list or view RAPs.
    """
