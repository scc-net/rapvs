"""Implement a cli module to view a remote access point"""

import click

from .._utils.types import MAC
from ...system import rap

MESSAGE = """mac: {}
S/N: {}
Blocked: {}"""


@click.command(name='view')
@click.argument('mac', metavar='MAC', type=MAC(), required=False)
@click.pass_context
def cli(ctx, mac):
    """View a RAP from rapvs databse

    \b
    Parameters:
    MAC   MAC address of RAP
    """
    if mac is None:
        mac = click.prompt('Please enter a MAC address', type=MAC())

    db_connection = ctx.obj['DB']
    rap_data = rap.get_data(db_connection, mac)

    click.echo(MESSAGE.format(*rap_data))
