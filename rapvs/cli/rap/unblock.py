"""Implement a cli module to block a remote access point"""

import click

from .._utils.types import MAC
from ...system import rap


@click.command(name='unblock')
@click.argument('mac', metavar='MAC', type=MAC(), required=False)
@click.pass_context
def cli(ctx, mac):
    """Unblock a RAP

    \b
    Parameters:
    MAC   MAC address of RAP
    """
    if mac is None:
        mac = click.prompt('Please enter a MAC address', type=MAC())

    db_connection = ctx.obj['DB']
    rap.unblock(db_connection, mac)
