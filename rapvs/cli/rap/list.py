"""Implement a cli module to list remote access points"""

import click

from .._utils.helpers import cond_echo_via_pager
from ...system import rap


@click.command(name='list')
@click.pass_context
def cli(ctx):
    """List RAPs in rapvs databse.

    \b
    """

    db_connection = ctx.obj['DB']

    rap_list = [(mac, serial_number, 'blocked' if blocked else 'unblocked', assignment_id, application_id, profile)
                for mac, serial_number, blocked, assignment_id, application_id, profile in rap.list_(db_connection)]
    headers = ['MAC', 'S/N', 'State', 'Assignment ID', 'Application ID', 'Profile']

    cond_echo_via_pager(rap_list, headers)
