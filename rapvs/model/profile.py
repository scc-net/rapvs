"""Provide a model for rap profiles"""

from sqlalchemy import Column, Integer, String
from sqlalchemy.orm import relationship

from .base import Base


class Profile(Base):  # pylint: disable=R0903
    """Model for rap profiles"""
    __tablename__ = 'profile'
    id = Column(Integer, primary_key=True)
    name = Column(String, unique=True, nullable=False)
    vlan_id = Column(Integer, unique=True, nullable=False)
    assignment = relationship('Assignment', back_populates='profile')
    # comment =
