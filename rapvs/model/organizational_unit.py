"""Provide a model for organizational units"""

from sqlalchemy import Column, Integer, String
from sqlalchemy.orm import relationship

from .base import Base


class OrganizationalUnit(Base):  # pylint: disable=R0903
    """Model for organizational units"""
    __tablename__ = 'organizational_unit'
    id = Column(Integer, primary_key=True)
    name = Column(String, unique=True, nullable=False)
    abbreviation = Column(String, unique=True, nullable=False)
    application = relationship('Application', back_populates='ou')
    # comment =
