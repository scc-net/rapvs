"""Provide SQLAlchemy Base Class"""

# from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import declarative_base
# for migration to sqlalchemy 2.0

Base = declarative_base()
