"""Provide a model for assignments"""

from sqlalchemy import Column, ForeignKey, Integer, String, TIMESTAMP
from sqlalchemy.dialects.postgresql import MACADDR
# from sqlalchemy.ext.hybrid import hybrid_property
from sqlalchemy.orm import relationship, validates
from sqlalchemy.sql import func

from .base import Base


class Assignment(Base):
    """Model for assignments"""
    __tablename__ = 'assignment'
    id = Column(Integer, primary_key=True)
    creation_timestamp = Column(TIMESTAMP, server_default=func.now(), nullable=False)
    revoke_timestamp = Column(TIMESTAMP, default=None)
    application_id = Column(Integer, ForeignKey('application.id'), nullable=False, unique=True)
    application = relationship('Application', uselist=False, back_populates='assignment')
    rap_id = Column(Integer, ForeignKey('rap.id'), unique=True)
    rap = relationship('RAP', single_parent=True, uselist=False, back_populates='assignment')
    rap_mac = Column(MACADDR, nullable=False)
    rap_serial_number = Column(String, nullable=False)
    profile_id = Column(Integer, ForeignKey('profile.id'), default=None)
    profile = relationship('Profile', back_populates='assignment')

    @validates('revoke_timestamp')
    def validate_revoke_timestamp(self, _, timestamp):
        """Validation function for revoke_timestamp"""
        if not self.active:
            raise ValueError('Application is already withdrawn')
        return timestamp

    ''''@hybrid_property
    def id(self):  # pylint: disable=C0103
        """Return id"""
        return self.application_id'''

    @property
    def active(self):
        """Return active state"""
        return self.revoke_timestamp is None

    def withdraw(self):
        """Withdraw assignment"""
        self.rap = None
        self.profile = None
        self.revoke_timestamp = func.now()
