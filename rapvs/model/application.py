"""Provide a model for applications"""

from sqlalchemy import Column, ForeignKey, Integer, TIMESTAMP
from sqlalchemy.orm import relationship, validates
from sqlalchemy.sql import func

from .base import Base


class Application(Base):
    """Model for applications"""
    __tablename__ = 'application'
    id = Column(Integer, primary_key=True)
    creation_timestamp = Column(TIMESTAMP, server_default=func.now(), nullable=False)
    rejected_timestamp = Column(TIMESTAMP)
    ou_id = Column(Integer, ForeignKey('organizational_unit.id'))
    ou = relationship('OrganizationalUnit', back_populates='application')
    user_id = Column(Integer, ForeignKey('user.id'))
    user = relationship('User', back_populates='application')
    assignment = relationship('Assignment', uselist=False, back_populates='application')

    @validates('rejected_timestamp')
    def validate_rejected_timestamp(self, _, timestamp):
        """Validation function for rejected_timestamp"""
        if self.is_assigned:
            raise ValueError('Can not reject assigned application')
        if self.is_rejected:
            raise ValueError('Application is already rejected')
        return timestamp

    def reject(self):
        """Reject application"""
        self.rejected_timestamp = func.now()

    def remove_user(self):
        """Delete application"""
        self.user_id = None

    @property
    def is_assigned(self):
        """Return is_assigned state"""
        return self.assignment is not None

    @property
    def is_rejected(self):
        """Return is_rejected state"""
        return self.rejected_timestamp is not None
