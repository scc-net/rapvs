"""Dataclasses for Mobility Conductor"""
from dataclasses import dataclass


@dataclass(init=False)
class RAP:
    """Represents one RAP in the AP List"""
    name: str
    ap_type: str
    mac: str
    serial: str
    group: str

    def __init__(self, access_point: dict):
        self.name = str(access_point['Name'])
        self.ap_type = str(access_point['AP Type'])
        self.mac = str(access_point['Wired MAC Address'])
        self.serial = str(access_point['Serial #'])
        self.group = str(access_point['Group'])


@dataclass(init=False)
class AllowlistRAP:
    """Represents one RAP in the RAP Allowlist"""
    name: str
    mac: str
    group: str
    revoked: bool
    revoke_text: str

    def __init__(self, entry: dict):
        self.name = str(entry['AP-Name'])
        self.mac = str(entry['Name'])
        self.group = str(entry['AP-Group'])
        self.revoked = not entry['Enabled'] == 'Yes'
        self.revoke_text = str(entry['Revoke-Text']) if entry['Revoke-Text'] is not None else ''
