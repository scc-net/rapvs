"""Provide a model for remote access points"""

from sqlalchemy import Boolean, Column, Integer, String
from sqlalchemy.dialects.postgresql import MACADDR
from sqlalchemy.orm import relationship

from .base import Base


class RAP(Base):
    """Model for remote access points"""
    __tablename__ = 'rap'
    id = Column(Integer, primary_key=True)
    mac = Column(MACADDR, unique=True, nullable=False)
    serial_number = Column(String, unique=True, nullable=False)
    blocked = Column(Boolean, server_default='f', nullable=False)
    assignment = relationship('Assignment', back_populates='rap')

    def block(self):
        """Block RAP"""
        self.blocked = True

    def unblock(self):
        """Unblock RAP"""
        self.blocked = False

    def check_serial(self, serial_number):
        """Check if serial number matches the one of this RAP."""
        if self.serial_number != serial_number:
            raise ValueError(f'RAP with MAC "{self.mac}" is already known, but the given S/N "{serial_number}"'
                             f' does not match the S/N in the DB: {self.serial_number}')
