"""Provide a model for users"""

from sqlalchemy import Column, Integer, String
from sqlalchemy.orm import relationship

from .base import Base


class User(Base):  # pylint: disable=R0903
    """Model for users"""
    __tablename__ = 'user'
    id = Column(Integer, primary_key=True)
    account = Column(String, unique=True, nullable=False)
    name = Column(String, nullable=False)
    email = Column(String, nullable=False)
    application = relationship('Application', back_populates='user')
