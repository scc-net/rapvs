"""Implement main function to run rapvs cli"""

from .cli import cli as main

if __name__ == "__main__":
    main()  # pylint: disable=E1120
