from logging.config import fileConfig

from sqlalchemy import engine_from_config
from sqlalchemy import pool

from alembic import context
import yaml
import os


# this is the Alembic Config object, which provides
# access to the values within the .ini file in use.
config = context.config

# Interpret the config file for Python logging.
# This line sets up loggers basically.
if config.config_file_name is not None:
    fileConfig(config.config_file_name)

user_config_dir = os.path.join(os.path.expanduser('~/.config'), 'rapvs', 'config.yml')

for configpath in ['/etc/rapvs.yml', user_config_dir]:
    if os.path.isfile(os.path.expanduser(configpath)):
        config_yaml = configpath

if config_yaml is None:
    raise Exception("Config file not found")

with open(os.path.expanduser(config_yaml), 'r', encoding='utf-8') as configfile:
    config_dict = yaml.safe_load(configfile)

try:
    db_host = config_dict['postgresql'].get('host')
    db_port = config_dict['postgresql'].get('port')
    db_database = config_dict['postgresql']['database']
    db_user = config_dict['postgresql']['username']
    db_password = config_dict['postgresql']['password']
except KeyError as error:
    raise Exception(f"Error during parsing of config file values: {error}")

section = config.config_ini_section
try:
    config.set_section_option(section, "DB_USER", db_user)
    config.set_section_option(section, "DB_PASS", db_password)
    config.set_section_option(section, "DB_HOST", db_host)
    # config.set_section_option(section, "DB_PORT", str(db_port))
    config.set_section_option(section, "DB_NAME", db_database)
except TypeError as error:
    raise Exception(f"Error during setting config values for DB in alembic url: {error}")

# add your model's MetaData object here
# for 'autogenerate' support
# from myapp import mymodel
# target_metadata = mymodel.Base.metadata
target_metadata = None

# other values from the config, defined by the needs of env.py,
# can be acquired:
# my_important_option = config.get_main_option("my_important_option")
# ... etc.


def run_migrations_offline() -> None:
    """Run migrations in 'offline' mode.

    This configures the context with just a URL
    and not an Engine, though an Engine is acceptable
    here as well.  By skipping the Engine creation
    we don't even need a DBAPI to be available.

    Calls to context.execute() here emit the given string to the
    script output.

    """
    url = config.get_main_option("sqlalchemy.url")
    context.configure(
        url=url,
        target_metadata=target_metadata,
        literal_binds=True,
        dialect_opts={"paramstyle": "named"},
    )

    with context.begin_transaction():
        context.run_migrations()


def run_migrations_online() -> None:
    """Run migrations in 'online' mode.

    In this scenario we need to create an Engine
    and associate a connection with the context.

    """
    connectable = engine_from_config(
        config.get_section(config.config_ini_section, {}),
        prefix="sqlalchemy.",
        poolclass=pool.NullPool,
    )

    with connectable.connect() as connection:
        context.configure(
            connection=connection, target_metadata=target_metadata
        )

        with context.begin_transaction():
            context.run_migrations()


if context.is_offline_mode():
    run_migrations_offline()
else:
    run_migrations_online()
