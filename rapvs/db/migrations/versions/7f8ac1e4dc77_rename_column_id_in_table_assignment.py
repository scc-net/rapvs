"""rename column id in table assignment

Revision ID: 7f8ac1e4dc77
Revises:
Create Date: 2023-05-16 13:39:18.924960

"""
from alembic import op

# revision identifiers, used by Alembic.
revision = '7f8ac1e4dc77'
down_revision = None
branch_labels = None
depends_on = None


def upgrade() -> None:
    op.drop_constraint("assignment_pkey", "assignment", type_="primary")
    op.alter_column("assignment", "_id", nullable=False, new_column_name="id")
    op.create_primary_key("assignment_pkey", "assignment", ["id"])


def downgrade() -> None:
    op.drop_constraint("assignment_pkey", "assignment", type_="primary")
    op.alter_column("assignment", "id", nullable=False, new_column_name="_id")
    op.create_primary_key("assignment_pkey", "assignment", ["_id"])
