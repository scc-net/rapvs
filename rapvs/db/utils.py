"""Module for db helper functions"""


def result2dict(result) -> dict:
    """Converts sqlalchemy result object to dict"""
    return dict(zip(result.keys(), result))
