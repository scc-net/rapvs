"""Implements printing support."""
import tempfile

from cups import Connection
from data import Data


def print_file(cups: Connection, printer: str, filepath: str):
    """Print file from filepath"""
    cups.printFile(printer, filepath, title='RAPVS Printout', options={})


def print_data(cups: Connection, printer: str, data: Data):
    """Prints file from data-object"""
    with tempfile.NamedTemporaryFile() as tmpfile:
        data.save_to(tmpfile.name)
        print_file(cups, printer, tmpfile.name)
