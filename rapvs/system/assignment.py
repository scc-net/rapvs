"""Functions to managed organizational units"""

from sqlalchemy import select

from .application import RejectError, AssignedError
from ..model import Application, Assignment, Profile, RAP, User, OrganizationalUnit


def create(db_connection, application_id, mac, serial_number, profile_name=None):
    """Create assignment from application id"""

    session = db_connection.session

    application = session.execute(select(Application).where(Application.id == application_id)).scalars().one_or_none()

    if application is None:
        raise KeyError(f"Application {application_id} dos not exist")

    if application.rejected_timestamp:
        raise RejectError(f"Application {application_id} was already rejected on {application.rejected_timestamp}.")

    if application.assignment:
        raise AssignedError(f'Application {application_id} is already assigned.')

    rap = session.execute(select(RAP).where(RAP.mac == mac)).scalars().one_or_none()
    if rap is None:
        rap = RAP(mac=mac, serial_number=serial_number)
    else:
        rap.check_serial(serial_number)

    if profile_name:
        profile_db = session.execute(select(Profile).where(Profile.name == profile_name)).scalars().one_or_none()
        if profile_db is None:
            raise ValueError(f"Profile {profile_name} unknown")
    else:
        profile_db = None

    assignment = Assignment(application=application, rap=rap, rap_mac=rap.mac,
                            rap_serial_number=rap.serial_number, profile=profile_db)

    session.add(assignment)
    session.commit()

    return assignment.id


def list_(db_connection, list_all: bool = False):
    """List assignments"""

    session = db_connection.session
    query = select(
        Assignment.id,
        Assignment.creation_timestamp,
        Assignment.revoke_timestamp,
        User.account.label('user_account'),
        Assignment.rap_mac,
        Profile.name,
        Application.id
    )

    if list_all:
        assignment_list = query.outerjoin(Assignment.rap).join(Application).outerjoin(User).outerjoin(Profile)
    else:
        assignment_list = query.join(Assignment.rap).join(Application).join(User).outerjoin(Profile)

    return session.execute(assignment_list.order_by(Assignment.id)).all()


def get_data(db_connection, id_):
    """Get data for assignment document."""

    session = db_connection.session
    assignment_db = session.execute(
        select(Assignment, User, OrganizationalUnit, Profile.name).outerjoin(Profile).join(Application).join(User).join(
            OrganizationalUnit).where(Assignment.id == id_)).one()

    return assignment_db


def profile(db_connection, id_, profile_name):
    """Set rap profile for assignment"""

    session = db_connection.session
    assignment_db = session.execute(select(Assignment).where(Assignment.id == id_)).scalars().one()
    if profile_name is not None:
        profile_db = session.execute(select(Profile).where(Profile.name == profile_name)).scalars().one()
    else:
        profile_db = None
    assignment_db.profile = profile_db

    session.commit()


def replace_rap(db_connection, old_mac, new_mac, new_serial_number):
    """Replaces assigned RAP"""
    session = db_connection.session

    # pylint: disable=C0121
    assignment = session.execute(select(Assignment).outerjoin(Profile).join(RAP)
                                 .where(RAP.mac == old_mac, Assignment.revoke_timestamp == None) # noqa E711
                                 ).scalars().one_or_none()

    if assignment is None:
        raise KeyError(f'No active assignment for RAP with the MAC {old_mac} is known.')

    new_rap = session.execute(select(RAP).where(RAP.mac == new_mac)).scalars().one_or_none()
    if new_rap is None:
        new_rap = RAP(mac=new_mac, serial_number=new_serial_number)
    else:
        new_rap.check_serial(new_serial_number)

    assignment.rap = new_rap
    assignment.rap_mac = new_mac
    assignment.rap_serial_number = new_serial_number

    session.commit()
    return assignment.id


def withdraw(db_connection, mac) -> Assignment:
    """Withdraw assignment"""

    session = db_connection.session
    # pylint: disable=C0121
    assignment = session.execute(select(Assignment).outerjoin(Profile).join(RAP).
                                 where(RAP.mac == mac, Assignment.revoke_timestamp == None)).scalars().one()  # noqa E711
    assignment.withdraw()

    session.commit()
    return assignment
