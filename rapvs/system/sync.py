"""Functions to sync database with mobility conductor"""
from typing import Callable

from .mobility_conductor import ArubaAPI
from ..db.db import DB
from .rap import list_ as rap_list


def format_rap_name(identifier: int) -> str:
    """Formats Assignment Identifier to RAP Name"""
    return f'war-{identifier:0>5d}'


# pylint: disable=R0912
def mobility_conductor(db_connection: DB, md_api: ArubaAPI, default_group: str,
                       test_mode: bool = True, echo_func: Callable = lambda s: None):
    """Actual synchronization logic.
    WHAT IT DOES:
        * Adding/Removing RAPs to MC DB/Allowlist
        * Renaming RAPs
    NOT SUPPORTED YET:
        * Group changes
        * Disable blocked APs
    WARNING:
        This was tested with a small dataset.
        It is possible that this code performs very bad with large datasets.
    """

    # get "should be" information
    rap_db = rap_list(db_connection)

    mc_rap_list = {rap.mac: rap for rap in md_api.rap_list.list()}
    mc_rap_allowlist = {rap.mac: rap for rap in md_api.rap_allowlist.list()}

    for entry in rap_db:
        mac = entry[0]
        assignment_id = entry[3]
        application_id = entry[4]
        profile = entry[5]

        # Delete not assigned RAPs from aplist/allowlist
        if assignment_id is None:
            if mac in mc_rap_allowlist.keys():
                echo_func(f'Delete RAP {mac} ({mc_rap_allowlist[mac].name}) from MC RAP Allowlist.')
                if not test_mode:
                    md_api.rap_allowlist.remove(mac)
            if mac in mc_rap_list.keys():
                echo_func(f'Delete RAP {mac} ({mc_rap_list[mac].name}) from MC RAP List.')
                if not test_mode:
                    md_api.rap_list.remove(mac)
            continue

        ap_name = format_rap_name(application_id)
        # Add new RAPs to Allowlist
        if mac not in mc_rap_allowlist.keys():
            echo_func(f'Adding {mac} as {ap_name} to Allowlist.')
            if not test_mode:
                md_api.rap_allowlist.add(mac, ap_name, profile or default_group)
            continue

        # Check RAP Allowlist Name
        rap_allowlist_name = mc_rap_allowlist[mac].name
        if ap_name != rap_allowlist_name:
            echo_func(f'Renaming RAP Allowlist Name {rap_allowlist_name} ({mac}) to {ap_name}')
            if not test_mode:
                md_api.rap_allowlist.modify(mac, ap_name)

        # Check RAP Configuration
        if mac in mc_rap_list.keys():
            # Check RAP Name
            rap_list_name = mc_rap_list[mac].name
            if ap_name != rap_list_name:
                echo_func(f'Renaming RAP {rap_list_name} ({mac}) to {ap_name}')
                if not test_mode:
                    md_api.rap_list.rename(mac, ap_name)
