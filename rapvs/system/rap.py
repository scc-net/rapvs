"""Functions to managed remote access points"""

from sqlalchemy import select

from ..model import RAP, Assignment, Profile, Application


def block(db_connection, mac):
    """block RAP"""

    session = db_connection.session
    rap_db = session.execute(select(RAP).where(RAP.mac == mac)).scalars().one()
    rap_db.block()
    session.commit()


def unblock(db_connection, mac):
    """unblock RAP"""

    session = db_connection.session
    rap_db = session.execute(select(RAP).where(RAP.mac == mac)).scalars().one()
    rap_db.unblock()
    session.commit()


def list_(db_connection):
    """List RAP"""

    session = db_connection.session
    rap_list = session.execute(select(
        RAP.mac,
        RAP.serial_number,
        RAP.blocked,
        Assignment.id,
        Application.id,
        Profile.name
    ).outerjoin(RAP.assignment).outerjoin(Assignment.application).outerjoin(Assignment.profile)).all()
    return rap_list


def get_data(db_connection, mac):
    """View RAP"""

    session = db_connection.session
    rap = session.execute(select(RAP.mac, RAP.serial_number, RAP.blocked).where(RAP.mac == mac)).one()
    return rap
