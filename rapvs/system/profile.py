"""Functions to managed remote access point profiles"""

from sqlalchemy import select

from ..model import Profile


def list_(db_connection):
    """List RAP profiles"""

    session = db_connection.session
    profile_list = session.execute(select(Profile.vlan_id, Profile.name)).all()
    return profile_list


def view(db_connection, name):
    """View RAP profile"""

    session = db_connection.session
    profile = session.execute(select(Profile.name, Profile.vlan_id).where(
        Profile.name == name)).one_or_none()
    # docs say to use scalars().one_or_none but perhaps does not work as expected

    if profile is None:
        raise IndexError(f"Profile {name} not found.")

    return profile.name, profile.vlan_id
