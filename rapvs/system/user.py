"""Functions to managed users"""

from sqlalchemy import select

from ..model import Application, User


def cleanup(db_connection):
    """Remove user from db"""
    session = db_connection.session
    application_db = session.execute(select(Application)).scalars().all()
    user_db = session.execute(select(User)).scalars().all()
    user_ids = list()
    application_user_ids = list()

    for usr in user_db:
        user_ids.append(usr.id)

    for application in application_db:
        application_user_ids.append(application.user_id)

    result = set(user_ids) - set(application_user_ids)
    deleted_users = list()
    for i in result:
        user_to_delete = session.execute(select(User).where(User.id == i)).scalars().one()
        deleted_users.append(user_to_delete)
        session.delete(user_to_delete)
        session.commit()

    return deleted_users
