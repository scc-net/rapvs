"""Functions to managed organizational units"""

from netdb_client.api32 import APISession
from netdb_client.api32 import org
from sqlalchemy import select

from ..model import OrganizationalUnit


class OUConflict(ValueError):
    """Raised if OU name conflicts"""


class OUNotFound(KeyError):
    """Raised if OU is not found"""


def add(db_connection, netdb_session: APISession, abbreviation):
    """Add a organizational unit to RAPVS databse"""

    session = db_connection.session

    netdb_ous = org.Unit.list(netdb_session, short_name_old=abbreviation)
    if not netdb_ous:
        raise OUNotFound
    ou_abbreviation = netdb_ous[0].short_name
    ou_name = netdb_ous[0].name

    ou_db = session.execute(
        select(OrganizationalUnit).where(OrganizationalUnit.abbreviation == ou_abbreviation)).scalars().first()

    if not ou_db:
        ou_ = OrganizationalUnit(abbreviation=ou_abbreviation, name=ou_name)
        session.add(ou_)
    elif ou_db.name != ou_name:
        message = f"Organizational name in RAPVS DB and NETVS DB are different: '{ou_db.name}' vs. {ou_name}'"
        raise OUConflict(message)
    else:
        ou_ = ou_db

    session.commit()
    return ou_


def list_(db_connection):
    """List organizational units"""

    session = db_connection.session
    ou_list = session.execute(select(OrganizationalUnit.abbreviation, OrganizationalUnit.name)).all()
    return ou_list
