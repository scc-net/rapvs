"""All aruba-mobility-conductor related functions"""
from aruba_api_caller import api_session

from ..model.mobility_conductor import RAP, AllowlistRAP


class ArubaAPIError(Exception):
    """Generic Aruba API Error"""

    def __init__(self, global_result):
        self.message = global_result['status_str']
        self.error_code = global_result['status']
        super().__init__(global_result)

    def __str__(self):
        return '{}. (API Error Code {})'.format(self.message.strip("\n"), self.error_code)


def parse_result(result):
    """Parse Aruba API results"""
    try:
        if result['_global_result']['status'] != 0:
            raise ArubaAPIError(result['_global_result'])
    except KeyError as key_error:
        raise Exception(f"Could not parse response: {result}") from key_error


class ArubaAPI:
    """Aruba API Wrapper Class"""

    def __init__(self, api_config):
        self.config_path = api_config['config_path']
        self.api = api_session(
            api_url=api_config['host'],
            username=api_config['username'],
            password=api_config['password'],
            check_ssl=api_config['check_ssl']
        )
        self.api.login()

        self.rap_list = self._RAPList(self.api, self.config_path)
        self.rap_allowlist = self._RAPAllowlist(self.api, self.config_path)

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.api.logout()

    class _RAPList:
        def __init__(self, session: api_session, config_path: str):
            self.session = session
            self.config_path = config_path

        def list(self):
            """List all RAPs on controllers"""
            raw = self.session.cli_command('show ap database long')
            remote_controllers = ['2a00:1398:9:fb0c::198', '2a00:1398:9:fb0c::199', '129.13.67.198', '129.13.67.199']
            rap_list = [RAP(ap) for ap in raw['AP Database'] if ap['Switch IP'] in remote_controllers]
            return rap_list

        def rename(self, mac: str, new_name: str):
            """Renames RAP"""
            res = self.session.post(
                api_path='configuration/object/ap_rename',
                config_path=self.config_path,
                data={'wired-mac': mac, 'new-name': new_name}
            )
            parse_result(res)

        def remove(self, mac: str):
            """Removes RAP from internal MC db
            Attention: API Call returns empty string"""
            self.session.cli_command(f'clear gap-db wired-mac {mac}')

    class _RAPAllowlist:
        def __init__(self, session, config_path: str):
            self.session = session
            self.config_path = config_path

        def list(self):
            """List all RAPs on Allowlist"""
            raw = self.session.cli_command('show whitelist-db rap')
            rap_allowlist = [AllowlistRAP(item) for item in raw['AP-entry Details']]
            return rap_allowlist

        def add(self, mac: str, ap_name: str, ap_group: str):
            """Adds entry to RAP Allowlist"""
            res = self.session.post(
                api_path='configuration/object/wdb_rap_add',
                config_path=self.config_path,
                data={'address': mac, 'ap_name': ap_name, 'ap_group': ap_group},
            )
            parse_result(res)

        def modify(self, mac: str, ap_name: str = None, ap_group: str = None):
            """Modify RAP Allowlist entry"""
            data = {'address': mac, 'ap_name': ap_name, 'ap_group': ap_group}
            data_sanitized = {key: value for key, value in data.items() if value is not None}
            res = self.session.post(
                api_path='configuration/object/wdb_rap_modify',
                config_path=self.config_path,
                data=data_sanitized
            )
            parse_result(res)

        def revoke(self, mac: str, comment: str = None):
            """Revokes RAP on Allowlist"""
            data = {'address': mac}
            if comment is not None:
                data['comment'] = comment
            res = self.session.post(
                api_path='configuration/object/wdb_rap_revoke',
                config_path=self.config_path,
                data=data
            )
            parse_result(res)

        def remove(self, mac: str):
            """Removes RAP from Allowlist"""
            res = self.session.post(
                api_path='configuration/object/wdb_rap_del',
                config_path=self.config_path,
                data={'address': mac}
            )
            parse_result(res)
