"""Implements mail sending functions"""
import smtplib
from email.message import EmailMessage

from datetime import timedelta
import humanize
from data import Data


def send_mail(mail: EmailMessage, mail_config: dict, application_data: dict):
    """Sends mail."""
    mail_from = f'{mail_config["from"]}'
    mail_to = f'{application_data["user_name"]} <{application_data["user_email"]}>'
    mail['From'] = mail_from
    mail['To'] = mail_to
    recipients = [mail_to]
    if 'bcc' in mail_config:
        if not isinstance(mail_config['bcc'], list):
            raise ValueError(f'Invalid Value for config key "bcc" in section "mail": {mail_config["bcc"]}')
        recipients.extend(mail_config['bcc'])
    smtpserver = smtplib.SMTP(mail_config['server'])
    smtpserver.ehlo()
    smtpserver.starttls()
    smtpserver.sendmail(mail_from, recipients, mail.as_string())
    smtpserver.close()


def application_mail(application_data, pdf: Data):
    """Create application mail."""
    mail = EmailMessage()
    mail['Subject'] = 'Ihr KIT-WLAN@Home Antrag'

    content = """Guten Tag,

der KIT-WLAN@Home-Antrag für Ihren Remote Access Point wurde bearbeitet.
Im Anhang finden Sie den Antrag als PDF. Bitte bringen Sie diesen und Ihren KIT-Mitarbeiter-Ausweis zur Abholung mit.
Termine und Orte zum Abholen des RAPs finden Sie auf unserer Website https://www.scc.kit.edu/dienste/wlan-at-home.

Mit freundlichen Grüßen
das SCC-WLAN-Team
"""

    mail.set_content(content)
    mail.add_attachment(
        pdf.readb(),
        maintype="application",
        subtype="pdf",
        filename=f'{application_data["id"]:0>5d}_application_{application_data["user_account"]}.pdf'
    )
    return mail


def application_rejected_mail(expiry_period_seconds=None):
    """Create application mail."""
    mail = EmailMessage()
    mail['Subject'] = 'Ihr KIT-WLAN@Home Antrag wurde gelöscht'

    if expiry_period_seconds is not None:
        humanize.activate(locale='de_DE')
        expiry_period = humanize.naturaldelta(timedelta(seconds=expiry_period_seconds))
        content = """Guten Tag,

Sie haben Ihren Remote Access Point {expiry} lang nicht abgeholt, damit ist der Antrag abgelaufen.
Wenn Sie dennoch einen Remote Access Point haben möchten, stellen Sie bitte erneut einen Antrag und kommen vor Ablauf
der {expiry}, um ihren Remote Access Point abzuholen.

Mit freundlichen Grüßen
das SCC-WLAN-Team
""".format(expiry=expiry_period)
    else:
        content = """Guten Tag,

Sie haben Ihren Remote Access Point nicht rechtzeitig abgeholt, damit ist der Antrag abgelaufen.
Wenn Sie dennoch einen Remote Access Point haben möchten, stellen Sie bitte erneut einen Antrag und kommen vor
Ablauf der Frist, um ihren Remote Access Point abzuholen.

Mit freundlichen Grüßen
das SCC-WLAN-Team
"""

    mail.set_content(content)
    return mail
