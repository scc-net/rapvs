"""PDF Generation Framework"""
from data import Data

from .compile import compile_pdf
from .template import render_template


def compile_application(application_data: dict) -> Data:
    """Creates application pdf"""
    tex_source = render_template('application.tex', application_data)
    pdf = compile_pdf(tex_source)
    return pdf


def compile_assignment(assignment_data: dict) -> Data:
    """Creates assignment pdf"""
    tex_source = render_template('assignment.tex', assignment_data)
    pdf = compile_pdf(tex_source)
    return pdf
