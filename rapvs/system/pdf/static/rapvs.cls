%% rapvs.cls
%% Created by Dominik Rimpf <dominik.rimpf@kit.edu>
\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{rapvs}[2021/01/22 RAPVS LaTeX Template class]

\LoadClass[a4paper,DIN,addrfield,parskip,enlargefirstpage]{scrlttr2}

\RequirePackage[ngerman]{babel}
\RequirePackage{fontspec}
\RequirePackage{hyperref}
\RequirePackage{graphicx}
\RequirePackage{qrcode}

% KIT Logo Position
\setplength{firstheadhpos}{20mm}
\setplength{firstheadvpos}{16mm}
\setplength{firstheadwidth}{42mm}
\setkomavar{firsthead}{\includegraphics[height=18mm]{kit_logo}}

% Arial or similar
\IfFontExistsTF{Arial}{
	\setmainfont{Arial}
	\setsansfont{Arial}
}{
	\setmainfont{Liberation Sans}
	\setsansfont{Liberation Sans}
}

\setplength{locvpos}{16mm}
\setplength{locwidth}{70mm}

\setkomavar{date}{ }


% Jinja2 Templating
\newcommand{\VAR}[1]{VAR}
\newcommand{\BLOCK}[1]{BLOCK}
