"""Implements all LaTeX Templating functions."""
import locale
import os.path
from datetime import datetime

from jinja2.loaders import FileSystemLoader
from latex.jinja2 import make_env

locale.setlocale(locale.LC_TIME, 'de_DE.UTF-8')
_template_dir = os.path.join(os.path.dirname(__file__), "templates")
env = make_env(loader=FileSystemLoader(os.path.join(_template_dir)))


def _tex_escape(string: str) -> str:
    """Escapes common problematic characters in TeX code like
    _, &."""
    tex_replacements = [
        ('{', '\\{'),
        ('}', '\\}'),
        ('[', '{[}'),
        (']', '{]}'),
        ('\\', '\\textbackslash{}'),
        ('$', '\\$'),
        ('%', '\\%'),
        ('&', '\\&'),
        ('#', '\\#'),
        ('_', '\\_'),
        ('~', '\\textasciitilde{}'),
        ('^', '\\textasciicircum{}'),
        ('<', '\\textless{}'),
        ('>', '\\textgreater{}'),
    ]
    mapping = dict((ord(char), rep) for char, rep in tex_replacements)
    escaped = str(string).translate(mapping)
    return escaped


def _format_date(date: datetime) -> str:
    return date.strftime('%d. %B %Y')


def _zero_pad(number):
    return f'{number:0>5d}'


def _barcode_mac(mac):
    return mac.replace(':', '')


def _getattr(obj, attr):
    return getattr(obj, attr)


env.filters['tex'] = _tex_escape
env.filters['format_date'] = _format_date
env.filters['barcode_mac'] = _barcode_mac
env.filters['pad'] = _zero_pad
env.filters['getattr'] = _getattr


def render_template(template: str, template_data: dict) -> str:
    """Renders template with jinja2"""
    tpl = env.get_template(template)
    rendered = tpl.render(**template_data)
    return rendered
