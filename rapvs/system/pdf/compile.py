"""Implements LaTeX Compile functions"""
import os.path

from data import Data
from latex import build_pdf

_static_dir = os.path.join(os.path.dirname(__file__), "static")


def compile_pdf(template: str) -> Data:
    """Takes rendered template and compiles it with all files in static dir"""
    pdf = build_pdf(template, texinputs=[_static_dir, ''], builder='xelatexmk')
    return pdf
