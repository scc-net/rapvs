"""LDAP System Module"""
import ldap3


def _ldap_account_filter(ldap_result, account):
    objects = []
    for object_ in ldap_result:
        if object_['type'] != 'searchResEntry':
            continue

        attributes = object_['attributes']

        account_ = attributes['sAMAccountName']
        if account_ != account:
            continue
        new_object = {'account': account_}

        try:
            sn_ = attributes['sn']
            gn_ = attributes['givenName']
            proxy_addresses = attributes['proxyAddresses']
        except KeyError as error:
            raise ValueError('Anonymous Account') from error
        if sn_ == account:
            raise ValueError('Anonymous Account')
        if gn_ == account:
            raise ValueError('Anonymous Account')
        name = f"{gn_} {sn_}"
        new_object['name'] = name

        active = not int(attributes['userAccountControl']) & 0x2
        new_object['active'] = active

        for address in proxy_addresses:
            if address.startswith('SMTP:'):
                new_object['email'] = address[5:]
                break

        objects.append(new_object)
    return objects


class LDAPConnection:
    """LDAP Connection Abstraction class"""

    def __init__(self, ldap_uri: str, ldap_username: str, ldap_password: str):
        ldap_server = ldap3.Server(ldap_uri)
        self.connection = ldap3.Connection(ldap_server, ldap_username, ldap_password, client_strategy=ldap3.SAFE_SYNC,
                                           auto_bind=True)

    def account2valid_user(self, basedn: str, account: str):
        """Get valid (active, not anonymous) account from account identifier"""
        ldap_filter = f"(sAMAccountName={account})"
        ldap_attributes = ["givenName", "sn", "sAMAccountName", "userAccountControl", "proxyAddresses"]
        status, result, response, _ = self.connection.search(basedn, ldap_filter, attributes=ldap_attributes)
        if not status:
            raise RuntimeError(f"LDAP search failed {result}")
        result_filtered = _ldap_account_filter(response, account)
        if not result_filtered:
            raise ValueError(f"Account {account} not found!")
        assert len(result_filtered) == 1

        ldap_user = result_filtered[0]
        if not ldap_user['active']:
            raise ValueError(f"Account {account} is disabled!")

        return ldap_user
