"""Functions to managed organizational units"""

from sqlalchemy import select

from ..model import Application, OrganizationalUnit, User, Assignment


class AssignedError(ValueError):
    """Raised if application already has an assignment"""


class RejectError(ValueError):
    """Raised if application is already rejected"""


# pylint: disable=R0913,R0914
def create(db_connection, user_name, user_account, user_email, organizational_unit) -> Application:
    """Create an application"""

    session = db_connection.session

    ou_db = session.execute(
        select(OrganizationalUnit).where(OrganizationalUnit.abbreviation == organizational_unit)).scalars().one()

    user_db = session.execute(select(User).where(User.account == user_account)).scalars().one_or_none()
    if user_db is None:
        user = User(account=user_account, name=user_name, email=user_email)
        session.add(user)
    else:
        user = user_db

    application = Application(ou=ou_db, user=user)

    session.add(application)
    session.commit()

    return application


def list_(db_connection):
    """List applications"""

    session = db_connection.session
    application_db = session.execute(
        select(Application, OrganizationalUnit, User, Assignment).join(OrganizationalUnit).join(User).outerjoin(
            Assignment)).all()

    return application_db


def get_data(db_connection, id_):
    """Get data for application document and view."""

    session = db_connection.session
    application = session.execute(
        select(Application, User, OrganizationalUnit).outerjoin(User).outerjoin(OrganizationalUnit).where(
            Application.id == id_)).one()

    return application


def reject(db_connection, id_):
    """Reject application"""

    session = db_connection.session

    application = session.execute(select(Application).where(Application.id == id_)).scalars().one()
    if application.is_rejected:
        raise RejectError(f"Application {application.id} was already rejected on {application.rejected_timestamp}.")
    application.reject()

    session.commit()


def remove_user(db_connection, id_):
    """Remove user_id from application"""

    session = db_connection.session
    application = session.execute(select(Application).where(Application.id == id_)).scalars().one()
    if application.is_rejected or not application.assignment.active:
        application.remove_user()
        session.commit()
